﻿using DMS.Entity;
using DMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMS.Repository
{
    public class FolderRepository : RepositoryBase<Folder>, IFolderRepository
    {
        public FolderRepository(IDatabaseFactory databaseFactory) :
            base(databaseFactory)
        {

        }
    }

    public interface IFolderRepository : IRepository<Folder>
    {

    }
}
