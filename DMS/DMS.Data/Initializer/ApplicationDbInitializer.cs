﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using DMS.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DMS.Data;

namespace DMS.Data
{
    public class ApplicationDbInitializer : CreateDatabaseIfNotExists<ApplicationDbContext>
    {
        protected override void Seed(ApplicationDbContext context)
        {
            InitializeIdentityForEF(context);
            context.Database.Initialize(true);
            base.Seed(context);
        }

        //Create User=Admin@Admin.com with password=Admin@123456 in the Admin role        
        public static void InitializeIdentityForEF(ApplicationDbContext context)
        {
            try
            {

                const string name = "superadmin@digitalagenepal.com";
                const string email = "superadmin@digitalagenepal.com";
                const string password = "Satellite@123456";
                const string roleName = "SuperAdmin";


                if (!context.Users.Any(user => string.Compare(user.UserName, name, StringComparison.CurrentCultureIgnoreCase) == 0))
                {
                    var userstore = new UserStore<ApplicationUser, ApplicationRole, Guid, ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>(context);
                    var usermanager = new UserManager<ApplicationUser, Guid>(userstore);

                    var administrator = new ApplicationUser
                    {
                        Id = Guid.Parse("DE69AA3E-CC18-430F-9818-6B7A45691ECF"),
                        FullName = "administrator",
                        UserName = name,
                        Email = email,
                        EmailConfirmed = true,
                        ADEnable = false,
                        ADUsername = "administrator",
                        

                    };

                    var approver = new ApplicationUser
                    {
                        Id = Guid.NewGuid(),
                        FullName = "authoriser",
                        UserName = "authoriser@digitalagenepal.com",
                        Email = "authoriser@digitalagenepal.com",
                        ADEnable = false,
                        ADUsername = "authoriser"

                    };

                    var inputer = new ApplicationUser
                    {
                        Id = Guid.NewGuid(),
                        FullName = "inputer",
                        UserName = "inputer@digitalagenepal.com",
                        Email = "inputer@digitalagenepal.com",
                        ADEnable = false,
                        ADUsername = "inputer"

                    };

                    usermanager.Create(administrator, password);
                    //usermanager.AddToRole(administrator.Id, roleName);

                    usermanager.Create(approver, password);
                    //usermanager.AddToRole(approver.Id, roleName);

                    usermanager.Create(inputer, password);
                    //usermanager.AddToRole(inputer.Id, roleName);


                    var rolestore = new RoleStore<ApplicationRole, Guid, ApplicationUserRole>(context);
                    var rolemanager = new RoleManager<ApplicationRole, Guid>(rolestore);

                    ApplicationRole _role = new ApplicationRole
                    {
                        Id = Guid.NewGuid(),
                        Name = roleName,
                        RoleCode = "001-SUP-RO",
                        Remarks = "Superadmin role for development."
                    };

                    rolemanager.Create(_role);




                    context.ApplicationUserRoles.Add(new ApplicationUserRole()
                    {
                        UserId = administrator.Id,
                        RoleId = _role.Id,
                        Remarks = "Superadmin role assigned"
                    });

                    context.ApplicationUserRoles.Add(new ApplicationUserRole()
                    {
                        UserId = inputer.Id,
                        RoleId = _role.Id,
                        Remarks = "Superadmin role assigned"

                    });

                    context.ApplicationUserRoles.Add(new ApplicationUserRole()
                    {
                        UserId = approver.Id,
                        RoleId = _role.Id,
                        Remarks = "Superadmin role assigned"
                    });




                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
