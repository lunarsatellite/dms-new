﻿using DMS.Data;
using System;
using System.Data.Entity;


namespace DMS.Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IDatabaseFactory databaseFactory;
        private ApplicationDbContext dataContext;

        public UnitOfWork(IDatabaseFactory databaseFactory)
        {
            this.databaseFactory = databaseFactory;
        }

        protected ApplicationDbContext DataContext
        {
            get { return dataContext ?? (dataContext = databaseFactory.Get()); }
        }

        public void Commit()
        {
           // var dataset = DataContext as DbContext;
            using (var dbContextTransaction = DataContext.DataBaseInfo.BeginTransaction())
            {
                try
                {

                    DataContext.Commit();
                    dbContextTransaction.Commit();
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException ex)
                {
                    dbContextTransaction.Rollback();
                    throw ex;
                }
            }

        }


        public async System.Threading.Tasks.Task<bool> CommitAsync()
        {
            try
            {
                await this.DataContext.CommitAsync();
                return true;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

    }
}
