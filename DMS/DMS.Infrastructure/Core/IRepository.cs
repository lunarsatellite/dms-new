﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DMS.Infrastructure
{
    public interface IRepository<T> where T : class
    {
        void Add(T entity);
        void Update(T entity);
        void Delete(T entity);
        Task Delete(Expression<Func<T, bool>> where);
        Task<T> GetById(long Id);
        Task<T> GetById(string Id);
        Task<T> GetById(Guid Id);
        T Get(Expression<Func<T, bool>> where);
        Task<T> GetAsync(Expression<Func<T, bool>> where);
        IEnumerable<T> GetAll();
        Task<List<T>> GetAllAsync();
        Task<IEnumerable<T>> GetMany(Expression<Func<T, bool>> where);
    }
}
