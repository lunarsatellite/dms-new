﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMS.Infrastructure.Core
{
    public interface IAuthenticationHelper
    {
        Guid GetUserId();
        string GetFullname();
        string GetBranch();
        
       
    }
}
