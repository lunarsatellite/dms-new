﻿using DMS.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace DMS.Infrastructure
{
    public abstract class RepositoryBase<T> where T : class
    {
        private ApplicationDbContext dataContext;
        private readonly DbSet<T> dbset;
        protected RepositoryBase(IDatabaseFactory databaseFactory)
        {
            DatabaseFactory = databaseFactory;
            dbset = DataContext.Set<T>();
        }

        protected IDatabaseFactory DatabaseFactory
        {
            get;
            private set;
        }

        protected ApplicationDbContext DataContext
        {
            get { return dataContext ?? (dataContext = DatabaseFactory.Get()); }
        }
        public virtual void Add(T entity)
        {
            dbset.Add(entity);
        }
        public virtual void Update(T entity)
        {
            dbset.Attach(entity);
            dataContext.Entry(entity).State = EntityState.Modified;
        }
        public virtual void Delete(T entity)
        {
            dbset.Attach(entity);
            dataContext.Entry(entity).State = EntityState.Deleted;
        }
        public virtual async Task Delete(Expression<Func<T, bool>> where)
        {
            IEnumerable<T> objects = await dbset.Where<T>(where).ToListAsync();
            foreach (T obj in objects)
                dbset.Remove(obj);
        }
        public async Task<T> GetById(long id)
        {
            return await dbset.FindAsync(id);
        }

        public async Task<T> GetById(string id)
        {
            return await dbset.FindAsync(id);
        }

        public virtual async Task<T> GetById(Guid id)
        {
            return await dbset.FindAsync(id);
        }

        public virtual IEnumerable<T> GetAll()
        {
            return dbset.ToList();
        }
        public virtual async Task<List<T>> GetAllAsync()
        {
            return await dbset.ToListAsync();
        }
        public virtual async Task<IEnumerable<T>> GetMany(Expression<Func<T, bool>> where)
        {
            return await dbset.Where(where).ToListAsync();
        }
        public Task<T> GetAsync(Expression<Func<T, bool>> where)
        {
            return dbset.FirstOrDefaultAsync(where);
        }
        public T Get(Expression<Func<T, bool>> where)
        {
            return dbset.Where(where).FirstOrDefault<T>();
        }
    }
}
