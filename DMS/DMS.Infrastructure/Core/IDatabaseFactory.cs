﻿using DMS.Data;
using System;
using System.Data.SqlClient;


namespace DMS.Infrastructure
{
    public interface IDatabaseFactory : IDisposable
    {
        ApplicationDbContext Get();
    }
}



