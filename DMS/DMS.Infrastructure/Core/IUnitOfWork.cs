﻿
using System.Threading.Tasks;
namespace DMS.Infrastructure
{
    public interface IUnitOfWork
    {
        void Commit();

        Task<bool> CommitAsync();

    }
}
