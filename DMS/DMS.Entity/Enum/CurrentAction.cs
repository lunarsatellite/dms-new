﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMS.Entity.Enum
{
    public enum CurrentAction
    {
        View = 1,
        Create,
        Edit,
        Delete,
        Revert,
        Discard,
        Authorise,
        AutoAuthorise,
        Download,
        Close
    }
}
