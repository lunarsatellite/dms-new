﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMS.Entity
{
    public enum RecordStatus
    {
        Active =1,
        Unauthorised,
        Inactive,        
        Closed,
        Reviewed,
        Reverted
    }
}
