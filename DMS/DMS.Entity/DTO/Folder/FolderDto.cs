﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMS.Entity
{
    public class FolderDto : BaseDto
    {
        [Required]
        [StringLength(250)]
        public string FolderName { get; set; }
        [Required]
        [StringLength(500)]
        public string Description { get; set; }

        public Nullable<Guid> ParentFolderId { get; set; }
    }
}
