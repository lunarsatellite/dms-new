﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMS.Entity
{
    public class BaseDto
    {
        [Key]
        public Guid Id { get; set; }
        [StringLength(200)]
        [Required]
        public string CreatedBy { get; set; }
        [Required]
        public DateTime CreatedDate { get; set; }
        [Required]
        public Guid CreatedById { get; set; }
        [StringLength(200)]
        [Required]
        public string ModifiedBy { get; set; }
        [Required]
        public DateTime ModifiedDate { get; set; }
        [Required]
        public Guid ModifiedById { get; set; }
        [StringLength(200)]
        public string AuthorisedBy { get; set; }
        public Nullable<DateTime> AuthorisedDate { get; set; }
        public Nullable<Guid> AuthorisedById { get; set; }
        [StringLength(200)]
        public string ReviewedBy { get; set; }
        public Nullable<DateTime> ReviewedDate { get; set; }
        public Nullable<Guid> ReviewedById { get; set; }
        public RecordStatus RecordStatus { get; set; }
        public int ModificationCounter { get; set; }
        [Column(TypeName = "xml")]
        public string ChangeLog { get; set; }
    }
}
