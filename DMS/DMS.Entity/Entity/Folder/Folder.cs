﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMS.Entity
{
    [Table(name:"Folder",Schema ="FolderManagement")]
    public class Folder : BaseEntity
    {
        public Folder()
        {

        }

        [Required]
        [StringLength(250)]
        public string FolderName { get; set; }
        [Required]
        [StringLength(500)]
        public string Description { get; set; }

        public Nullable<Guid> ParentFolderId { get; set; }

        [ForeignKey("ParentFolderId")]
        public virtual Folder ParentFolder { get; set; }


    }
}
