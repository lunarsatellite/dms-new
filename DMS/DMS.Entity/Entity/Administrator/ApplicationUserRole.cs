using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DMS.Entity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DMS.Entity
{
    [Table("ApplicationUserRole", Schema = "Administrator")]
    public class ApplicationUserRole : IdentityUserRole<Guid>
    {
        public ApplicationUserRole()
        {
        }

        [Required(ErrorMessage = "Remarks is required.")]
        [StringLength(300)]
        public string Remarks { get; set; }
    }
}
