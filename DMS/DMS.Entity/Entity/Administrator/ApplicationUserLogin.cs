﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMS.Entity
{
    [Table(name: "UserLogin", Schema = "Administrator")]
    public class ApplicationUserLogin : IdentityUserLogin<Guid>
    {
    }
}
