using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using DMS.Entity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DMS.Entity
{
    [Table("ApplicationUser", Schema = "Administrator")]
    public class ApplicationUser : IdentityUser<Guid, ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>, IUser<Guid>
    {
        
        [Required(ErrorMessage = "Fullname is required.")]
        [StringLength(500)]
        public string FullName { get; set; }
        [StringLength(10)]
        public string BranchCode { get; set; }
        [StringLength(256)]
        public string ADUsername { get; set; }
        [Required(ErrorMessage = "AD Enable is required.")]
        public bool ADEnable { get; set; }



        public async Task<ClaimsIdentity>
         GenerateUserIdentityAsync(UserManager<ApplicationUser, Guid> manager, string AuthenticationTypes)
        {
            var userIdentity = await manager
                .CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);

            userIdentity.AddClaim(new Claim(ClaimTypes.GivenName, this.FullName.ToString()));
            userIdentity.AddClaim(new Claim(ClaimTypes.UserData, string.Empty));


            foreach (var item in this.Claims)
            {
                userIdentity.AddClaim(new Claim(item.ClaimType, item.ClaimValue));

            }
            return userIdentity;
        }
    }
}
