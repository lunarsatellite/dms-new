using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DMS.Entity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DMS.Entity
{
    [Table("ApplicationRole", Schema = "Administrator")]
    public class ApplicationRole : IdentityRole<Guid, ApplicationUserRole>
    {
        public ApplicationRole()
        {

        }

        [Required(ErrorMessage = "Role Code is required.")]
        [StringLength(20)]
        public string RoleCode { get; set; }
        [Required(ErrorMessage = "Remarks is required.")]
        [StringLength(300)]
        public string Remarks { get; set; }
    }
}
